package hibernateTest.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import hibernateTest.entity.Address;
import hibernateTest.entity.Employee;

public class HibernateUtils {
	private static final SessionFactory sessionFactory= buildSession();
	private static final EntityManagerFactory entityManagerFactory = buildEntityManagerFactory();

	private static SessionFactory buildSession() {	
		//Map<String, String> settings = new HashMap<String, String>();
		Properties settings = new Properties();
		settings.setProperty(Environment.DRIVER, "com.mysql.jdbc.Driver");
		settings.setProperty(Environment.URL, "jdbc:mysql://localhost:3306/test");
		settings.setProperty(Environment.USER, "root");
		settings.setProperty(Environment.PASS, "root");
		settings.setProperty(Environment.DIALECT, "org.hibernate.dialect.MySQL57Dialect");
		settings.setProperty(Environment.CACHE_REGION_FACTORY, "org.hibernate.cache.ehcache.EhCacheRegionFactory");

		 StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
           .applySettings(settings).build();
         Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        // return metadata.getSessionFactoryBuilder().build();
         
		//return new Configuration().configure().buildSessionFactory();
       return new Configuration().addProperties(settings).addAnnotatedClass(Employee.class)
    		   .addAnnotatedClass(Address.class)
    		   .buildSessionFactory();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void shutDown() {
		sessionFactory.close();
	}

	private static EntityManagerFactory buildEntityManagerFactory() {
		return Persistence.createEntityManagerFactory("JPAUnit");
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void shutDownEMF() {
		entityManagerFactory.close();
	}

	private static StandardServiceRegistry registry;
	private static SessionFactory sessionFactory2;

	public static SessionFactory getSessionFactory2() {
		if (sessionFactory2 == null) {
			try {

				// Create registry builder
				StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Map<String, String> settings = new HashMap<String, String>();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/test");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL57Dialect");

				// Apply settings
				registryBuilder.applySettings(settings);

				// Create registry
				registry = registryBuilder.build();

				// Create MetadataSources
				MetadataSources sources = new MetadataSources(registry);

				// Create Metadata
				Metadata metadata = sources.getMetadataBuilder().build();

				// Create SessionFactory
				sessionFactory2 = metadata.getSessionFactoryBuilder().build();

			} catch (Exception e) {
				e.printStackTrace();
				if (registry != null) {
					StandardServiceRegistryBuilder.destroy(registry);
				}
			}
		}
		return sessionFactory;
	}

	public static void shutdown() {
		if (registry != null) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}
}
