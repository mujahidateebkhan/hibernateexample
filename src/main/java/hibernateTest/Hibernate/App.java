package hibernateTest.Hibernate;

import javax.persistence.EntityManager;

//import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.query.Query;

import hibernateTest.entity.Employee;
import hibernateTest.utils.HibernateUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
    	EntityManager em = new HibernateUtils().getEntityManagerFactory().createEntityManager();
    	EntityManager em1 = new HibernateUtils().getEntityManagerFactory().createEntityManager();

        System.out.println( "Hello World!" );
        Session session = new HibernateUtils().getSessionFactory().openSession();
        Session session2 = new HibernateUtils().getSessionFactory().openSession();
        Session session3 = new HibernateUtils().getSessionFactory().openSession();
		Session session4 = HibernateUtils.getSessionFactory2().openSession();

        session.getTransaction().begin();

        Employee test  = new Employee();
        test.setEmail("Raj@gmail.com");
        test.setName("Raj");
        test.setPhone("9877897999");
        session.save(test);
        session.getTransaction().commit();
        session.close();
        
        /** Save Method*/
       /* session.getTransaction().begin();
        Long id = (Long) session.save(test); //Returns Id
        System.out.println(id +" 1 Save");
        test.setEmail("ateeb@gmail.com");
        id = (Long) session.save(test); //Update Email Before Committing if session is commit else its not updates email
        System.out.println(id +" 2 Save");
        //session.getTransaction().commit();
        //session.evict(test); //Object is detached
        //session.save(test);//Generates After Detaching Id New Id and Updates Email
        session.getTransaction().commit();
        session.getTransaction().begin();
        session.evict(test);
        id = (Long) session.save(test);
        System.out.println(id +" 3 Save");
        session.getTransaction().commit();*/
        
        
        /** Persit Method*/
        /*session.getTransaction().begin();
        session.persist(test); //Returns Void
        session.getTransaction().commit();
        session.getTransaction().begin();
        session.evict(test);
        session.persist(test); // Got javax.persistence.PersistenceException
        session.getTransaction().commit();*/
        
        /** Merge Method*/
       /* session.getTransaction().begin();
        session.save(test);
        session.getTransaction().commit();
        session.getTransaction().begin();
        session.evict(test);
        test.setPhone("8088017757");
        session.merge(test); //Updates Phone Even after Object is detached before Updating It fires the Select Statement
        session.getTransaction().commit();*/
        
        /** SaveUpdate Method */
       /* session.getTransaction().begin();
        session.save(test); //Returns Void 
        session.getTransaction().commit();
        session.getTransaction().begin();
        session.evict(test);
        test.setName("Khan");
        session.saveOrUpdate(test);
        session.getTransaction().commit();*/
        
        
        /** Get Object By Load Method*/
        /*Employee load1 = session.load(Employee.class, 1L);
        System.out.println("Load Data Id: "+ load1.getId());*/
        //System.out.println("Load Data Email: "+ load1.getEmail()); //just returns the reference of an object that might not actually exists, it loads the data from database or cache only when you access other properties of the object.
        //Employee load2 = session.load(Employee.class, 16L);
        //System.out.println(load1.getEmail());
        //System.out.println(load2.getEmail()); // Throws org.hibernate.ObjectNotFoundException
        
        
        /** Get Object By Get Method*/
        /*System.out.println("Get Data-----------");
        Employee load3 = session.get(Employee.class, 2L); //returns the object by fetching it from database or from hibernate cache
        System.out.println("Get Data Id: "+ load3.getId());
        System.out.println("Get Data Email: "+ load3.getEmail());*/
        //Employee load4 = session.get(Employee.class, 16L); 
        //System.out.println(load3.getEmail());
        //System.out.println(load4.getEmail());//Throws Null Pointer Exception
        
        
        /*session.getTransaction().begin();
        session.save(test); //Returns Void 
        test.setName("Khan");
        session2.getTransaction().commit(); // Throws  java.lang.IllegalStateException
        */
        
        /*Employee load1 = session.get(Employee.class, 15L);
        System.out.println(load1.getEmail());
        Thread.sleep(10000);
        System.out.println("***************Cache Data Loads Here**************");
        Employee load2 = session.get(Employee.class, 15L); //When We Load the data again Select Query Not Fire Again
        System.out.println(load2.getEmail());
        session.clear();
        Thread.sleep(10000);
        System.out.println("***************When Clears Sessions and Again Loads Here**************");
        Employee load3 = session.get(Employee.class, 15L);//Select Query Fires Here
        System.out.println(load3.getEmail());*/
        
        /** Second Level Caching*/
        /*session.getTransaction().begin();
        Employee emp = session.get(Employee.class, 1L);
        System.out.println(emp);
        session.getTransaction().commit();
        session.close();
        
        session2.getTransaction().begin();
        Employee emp1 = session2.get(Employee.class, 1L);
        System.out.println(emp1);
        session2.getTransaction().commit();
        session2.close();
        
        session3.getTransaction().begin();
        Employee test  = new Employee();
        test.setId(1L);
        test.setEmail("ateeb79@gmail.com");
        test.setName("Mujahid Ateeb Khan");
        test.setPhone("9535079919");
        session3.merge(test);
        session3.getTransaction().commit();
        session3.close();
        
        session3 = new HibernateUtils().getSessionFactory().openSession();
        session3.getTransaction().begin();
        Employee emp2 = session3.get(Employee.class, 1L);
        System.out.println(emp2);
        session3.getTransaction().commit();
        session3.close();
        */
        
        /**Query Caching*/
        /*session.getTransaction().begin();
        Query<?> q = session.createQuery("from Employee where id=1");
        q.setCacheable(true);
        Employee emp = (Employee) q.getSingleResult();
        System.out.println(emp);
        session.getTransaction().commit();
        session.close();
        
        session2.getTransaction().begin();
        Query<?> q1 = session2.createQuery("from Employee where id=1");
        q1.setCacheable(true);
        Employee emp1 = (Employee) q1.getSingleResult();
        System.out.println(emp1);
        //session2.getTransaction().commit();
        session2.close();*/
        
        em.getTransaction().begin();
        Employee e = em.find(Employee.class, 1L);
        System.out.println(e);
        em.getTransaction().commit();
        em.close();
        
        em1.getTransaction().begin();
        Employee e1 = em1.find(Employee.class, 1L);
        System.out.println(e1);
        em1.getTransaction().commit();
        em1.close();
    }
}
